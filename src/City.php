<?php
include "log.php";
require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload
class City
{
   public function getCityNameById($id)
   {
       if ($id == 1) {
           $city = "Bordeaux";
       } else {
           $city = "Paris";
       }
       return $city;
   }
}
